package com.example.demo.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class CalculateGradeTest {

//	@Autowired
//	CalculateGrade calculateGrade;
	CalculateGrade calculateGrade = new CalculateGrade();

	@Test
	public void checkA1() throws Exception {
		assertEquals("A", calculateGrade.getGrade(90));
	}
	
	@Test
	public void checkA() throws Exception {
		assertEquals("A", calculateGrade.getGrade(90));
	}

	@Test
	public void checkB() throws Exception {
		assertEquals("B", calculateGrade.getGrade(70));
	}
	
	@Test
	public void checkC() throws Exception {
		assertEquals("C", calculateGrade.getGrade(60));
	}
	
	@Test
	public void checkD() throws Exception {
		assertEquals("D", calculateGrade.getGrade(50));
	}
	
	@Test
	public void checkF() throws Exception {
		assertEquals("F", calculateGrade.getGrade(40));
	}
}
