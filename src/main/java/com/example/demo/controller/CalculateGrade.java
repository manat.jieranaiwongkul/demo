package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class CalculateGrade {

	 @GetMapping("/grade/{point}")
	    public String getGrade(@PathVariable(value = "point") int score)
	        throws Exception {
		 if (score >= 80) {
			 return "A";
		 }else if (score < 79 && score >= 70) {
			 return "B";
		 }else if (score < 69 && score >= 60) {
			 return "C";
		 }else if (score < 59 && score >= 50) {
			 return "D";
		 }else if (score < 50 ) {
			 return "F";
		 }
	      
		 return null;
	    }
}
